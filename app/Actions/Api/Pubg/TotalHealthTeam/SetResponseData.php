<?php

namespace App\Actions\Api\Pubg\TotalHealthTeam;

class SetResponseData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $teams = $request->teams;

        $request->merge([
            'data_response' => $teams->toArray(),
        ]);
    }
}
