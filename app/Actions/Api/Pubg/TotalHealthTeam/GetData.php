<?php

namespace App\Actions\Api\Pubg\TotalHealthTeam;

use App\Models\Player;
use DB;

class GetData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $teams = Player::select(DB::raw('team_id, SUM(health) as total_health_team'))
            ->orderBy('team_id', 'asc')
            ->groupBy('team_id')
            ->get();

        $request->merge(['teams' => $teams]);
    }
}
