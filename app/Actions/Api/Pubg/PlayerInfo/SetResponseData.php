<?php

namespace App\Actions\Api\Pubg\PlayerInfo;

use App\Models\Player;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SetResponseData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $result = [];
        $players = $request->players;
        $fractal = new Manager;

        if ($players->count() > 0) {
            $playersArray = $players->toArray();
            $resource = new Collection($playersArray, function (array $p) {
                return [
                    'teamName' => $p['team_name'],
                    'teamId' => (int) $p['team_id'],
                    'health' => $p['health'],
                    'healthMax' => $p['health_max'],
                    'killNum' => $p['kill_num'],
                    'liveState' => $p['live_state'],
                    'playerName' => $p['name'],
                    'playerKey' => $p['player_key'],
                    'playerOpenId' => $p['player_open_id'],
                    'uId' => $p['uid'],
                    'rank' => $p['rank'],
                    'total_kill_num_team' => (int) self::totalKillNumTeam($p['team_id']),
                    'total_health_team' => (int) self::totalHealthTeam($p['team_id']),
                    'useBurnGrenadeNum' => $p['use_smoke_grenade_num'],
                    'useFlashGrenadeNum' => $p['use_frag_grenade_num'],
                    'useFragGrenadeNum' => $p['use_burn_grenade_num'],
                    'useSmokeGrenadeNum' => $p['use_flash_grenade_num'],
                ];
            });
            $array = $fractal->createData($resource)->toArray();
            $result = $array['data'];
        }

        $request->merge(['data_response' => [
            'playerInfoList' => $result,
        ]]);
    }

    private static function totalKillNumTeam($teamId)
    {
        $total_kill_num_team = Player::where('team_id', $teamId)->sum('kill_num');

        return $total_kill_num_team;
    }

    private static function totalHealthTeam($teamId)
    {
        $total_health_team = Player::where('team_id', $teamId)->sum('health');

        return $total_health_team;
    }
}
