<?php

namespace App\Actions\Api\Pubg\PlayerInfo;

class Handler
{
    public function handle(\Illuminate\Http\Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            GetData::handle($request);
            SetResponseData::handle($request);

            return response()->pubg(200, $request->data_response);
        } catch (\Exception $e) {
            return response()->pubg(400, []);
        }
    }
}
