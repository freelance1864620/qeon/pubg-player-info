<?php

namespace App\Actions\Api\Pubg\PlayerInfo;

use App\Models\Player;

class GetData
{
    public static function handle(\Illuminate\Http\Request $request)
    {
        $players = Player::orderBy('team_id', 'asc')->get();
        $request->merge(['players' => $players]);
    }
}
