<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ActionApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->pubg();
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }

    public function pubg()
    {
        $this->app->bind(
            'app.action.api.pubg.player-info',
            \App\Actions\Api\Pubg\PlayerInfo\Handler::class
        );

        $this->app->bind(
            'app.action.api.pubg.total-health-team',
            \App\Actions\Api\Pubg\TotalHealthTeam\Handler::class
        );
    }
}
