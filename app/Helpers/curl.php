<?php

if (! function_exists('CurlGet')) {
    function CurlGet(string $url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result === false) {
            $error = curl_error($ch);
            throw new \Exception($error);
        }

        return $result;
    }
}
