<?php

namespace App\Console\Commands;

use App\Models\Player;
use Illuminate\Console\Command;

class CrawlPlayerInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:crawl-player-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->crawl();
    }

    public function crawl()
    {
        try {
            $url = config('qeon.url_data_source');
            $response = CurlGet($url);

            if (isJson($response) == false) {
                throw new \Exception('Data source is not json format');
            }

            $data = json_decode($response, true);
            if (is_array($data) == false) {
                throw new \Exception('Data source is not array format');
            }

            if (! array_key_exists('playerInfoList', $data)) {
                throw new \Exception('Key playerInfoList not found in data source');
            }

            $playerInfoList = $data['playerInfoList'];
            if (is_array($playerInfoList) == false) {
                throw new \Exception('Data source is not contain playerInfoList');
            }

            if (count($playerInfoList) == 0) {
                throw new \Exception('Data source is empty');
            }
            foreach ($playerInfoList as $playerInfo) {
                $this->validateKeys($playerInfo);
                $this->savePlayer($playerInfo);
            }
        } catch (\Exception $e) {
            echo 'Error: '.$e->getMessage().PHP_EOL;
        }
    }

    public function validateKeys($data)
    {
        if (array_key_exists('teamName', $data) == false) {
            throw new \Exception('Key teamName not found in data source');
        }

        if (array_key_exists('teamId', $data) == false) {
            throw new \Exception('Key teamId not found in data source');
        }

        if (array_key_exists('health', $data) == false) {
            throw new \Exception('Key health not found in data source');
        }

        if (array_key_exists('healthMax', $data) == false) {
            throw new \Exception('Key healthMax not found in data source');
        }

        if (array_key_exists('rank', $data) == false) {
            throw new \Exception('Key rank not found in data source');
        }

        if (array_key_exists('killNum', $data) == false) {
            throw new \Exception('Key killNum not found in data source');
        }

        if (array_key_exists('liveState', $data) == false) {
            throw new \Exception('Key liveState not found in data source');
        }

        if (array_key_exists('playerName', $data) == false) {
            throw new \Exception('Key playerName not found in data source');
        }

        if (array_key_exists('playerKey', $data) == false) {
            throw new \Exception('Key playerKey not found in data source');
        }

        if (array_key_exists('playerOpenId', $data) == false) {
            throw new \Exception('Key playerOpenId not found in data source');
        }

        if (array_key_exists('uId', $data) == false) {
            throw new \Exception('Key uId not found in data source');
        }

        if (array_key_exists('useBurnGrenadeNum', $data) == false) {
            throw new \Exception('Key useBurnGrenadeNum not found in data source');
        }

        if (array_key_exists('useFlashGrenadeNum', $data) == false) {
            throw new \Exception('Key useFlashGrenadeNum not found in data source');
        }

        if (array_key_exists('useFragGrenadeNum', $data) == false) {
            throw new \Exception('Key useFragGrenadeNum not found in data source');
        }

        if (array_key_exists('useSmokeGrenadeNum', $data) == false) {
            throw new \Exception('Key useSmokeGrenadeNum not found in data source');
        }
    }

    public function savePlayer($data)
    {
        $playerExist = Player::where('name', $data['playerName'])
            ->where('uid', $data['uId'])
            ->where('player_key', $data['playerKey'])
            ->first();

        if ($playerExist) {
            $playerExist->update([
                'health' => $data['health'],
                'health_max' => $data['healthMax'],
                'kill_num' => $data['killNum'],
                'live_state' => $data['liveState'],
                'rank' => $data['rank'],
                'team_id' => $data['teamId'],
                'team_name' => $data['teamName'],
                'use_burn_grenade_num' => $data['useBurnGrenadeNum'],
                'use_flash_grenade_num' => $data['useFlashGrenadeNum'],
                'use_frag_grenade_num' => $data['useFragGrenadeNum'],
                'use_smoke_grenade_num' => $data['useSmokeGrenadeNum'],
            ]);
        } else {
            Player::create([
                'uid' => $data['uId'],
                'player_open_id' => $data['playerOpenId'],
                'name' => $data['playerName'],
                'player_key' => $data['playerKey'],
                'health' => $data['health'],
                'health_max' => $data['healthMax'],
                'kill_num' => $data['killNum'],
                'live_state' => $data['liveState'],
                'rank' => $data['rank'],
                'team_id' => $data['teamId'],
                'team_name' => $data['teamName'],
                'use_burn_grenade_num' => $data['useBurnGrenadeNum'],
                'use_flash_grenade_num' => $data['useFlashGrenadeNum'],
                'use_frag_grenade_num' => $data['useFragGrenadeNum'],
                'use_smoke_grenade_num' => $data['useSmokeGrenadeNum'],
            ]);
        }
    }
}
