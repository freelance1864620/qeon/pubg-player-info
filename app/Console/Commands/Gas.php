<?php

namespace App\Console\Commands;

use App\Models\Player;
use App\Models\Setting;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Gas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:gas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        while (true) {
            try {
                $truncateData = Setting::where('key', 'truncate_data')->first();
                if ($truncateData->value == 'yes') {
                    Artisan::call('app:clean-data');
                    echo 'truncate : ' . $truncateData->value.PHP_EOL;
                } else {
                    echo 'truncate : no ' .PHP_EOL;
                }

                $running = Setting::where('key', 'is_running')->first();
                if ($running->value == 'yes') {
                    echo 'running : ' .$running->value.PHP_EOL;
                    Artisan::call('app:crawl-player-info');
                } else {
                    echo 'running no ' .PHP_EOL;
                }
            } catch (\Exception $e) {
                echo 'Error: '.$e->getMessage().PHP_EOL;
            }
        }
    }
}
