<?php

namespace App\Console\Commands;

use App\Models\Setting;
use Illuminate\Console\Command;

class ChangeStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:change-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        while (true) {
            try {
                $statusTruncate = $this->getStatusTruncate();
                echo 'last status truncate : ' . $statusTruncate.PHP_EOL;
                Setting::where('key', 'truncate_data')->update(['value' => $statusTruncate]);
                $statusRunning = $this->getStatusRunning();
                echo 'last status running : ' . $statusRunning.PHP_EOL;
                Setting::where('key', 'is_running')->update(['value' => $statusRunning]);
                sleep(5);
            } catch (\Exception $e) {
                echo 'Error: '.$e->getMessage().PHP_EOL;
            }
        }
    }

    public function getStatusRunning()
    {
        $statusRunning = 'https://helperpubg.la790x.xyz/api/last-status-is-running';
        $clientStatusRunning = new \GuzzleHttp\Client;
        $responseStatusRunning = $clientStatusRunning->request('GET', $statusRunning, ['timeout' => 10]);
        $bodyResponseResponseStatusRunning = $responseStatusRunning->getBody()->getContents();
        $bodyResponseResponseStatusRunningArray = json_decode($bodyResponseResponseStatusRunning, true);

        return $bodyResponseResponseStatusRunningArray['message'];
    }

    public function getStatusTruncate()
    {
        $statusTruncate = 'https://helperpubg.la790x.xyz/api/last-status-truncate';
        $clientStatusTruncate = new \GuzzleHttp\Client;
        $responseStatusTruncate = $clientStatusTruncate->request('GET', $statusTruncate, ['timeout' => 10]);
        $bodyResponseResponseStatusTruncate = $responseStatusTruncate->getBody()->getContents();
        $bodyResponseResponseStatusTruncateArray = json_decode($bodyResponseResponseStatusTruncate, true);

        return $bodyResponseResponseStatusTruncateArray['message'];
    }
}
