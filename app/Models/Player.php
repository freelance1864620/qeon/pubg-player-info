<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Player
 *
 * @property int $id
 * @property string $uid
 * @property string $name
 * @property string $player_open_id
 * @property string $player_key
 * @property int $health
 * @property int $health_max
 * @property int $live_state
 * @property int $kill_num
 * @property int $rank
 * @property int $team_id
 * @property string $team_name
 * @property int $use_smoke_grenade_num
 * @property int $use_frag_grenade_num
 * @property int $use_burn_grenade_num
 * @property int $use_flash_grenade_num
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Player extends Model
{
    protected $table = 'players';

    protected $casts = [
        'health' => 'int',
        'health_max' => 'int',
        'live_state' => 'int',
        'kill_num' => 'int',
        'rank' => 'int',
        'team_id' => 'int',
        'use_smoke_grenade_num' => 'int',
        'use_frag_grenade_num' => 'int',
        'use_burn_grenade_num' => 'int',
        'use_flash_grenade_num' => 'int',
    ];

    protected $fillable = [
        'uid',
        'name',
        'player_open_id',
        'player_key',
        'health',
        'health_max',
        'live_state',
        'kill_num',
        'rank',
        'team_id',
        'team_name',
        'use_smoke_grenade_num',
        'use_frag_grenade_num',
        'use_burn_grenade_num',
        'use_flash_grenade_num',
    ];
}
