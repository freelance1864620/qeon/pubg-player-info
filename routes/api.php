<?php

use Illuminate\Support\Facades\Route;

Route::any('/hello', function () {
    return response()->api(200, 'success', [], 'Hello..');
});

Route::prefix('pubg')->as('api.pubg.')->group(__DIR__.'/api/pubg.php');
