<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => []], function () {
    Route::get('player-info', function (Request $request) {
        return app('app.action.api.pubg.player-info')->handle($request);
    })->name('player-info');
    Route::get('total-health-team', function (Request $request) {
        return app('app.action.api.pubg.total-health-team')->handle($request);
    })->name('total-health-team');
});
