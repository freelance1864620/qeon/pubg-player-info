<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string('uid');
            $table->string('name');
            $table->string('player_open_id');
            $table->string('player_key');
            $table->integer('health');
            $table->integer('health_max');
            $table->integer('live_state');
            $table->integer('kill_num');
            $table->integer('rank');
            $table->integer('team_id');
            $table->string('team_name');
            $table->integer('use_smoke_grenade_num');
            $table->integer('use_frag_grenade_num');
            $table->integer('use_burn_grenade_num');
            $table->integer('use_flash_grenade_num');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('players');
    }
};
